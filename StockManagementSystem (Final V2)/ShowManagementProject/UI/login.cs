﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;
using ShowManagementProject.BLL;
using ShowManagementProject.Model;


namespace ShowManagementProject
{
    public partial class login : Form
    {      

        public login()
        {
            InitializeComponent();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();     //close all page
        }


        LoginManager aLoginManager=new LoginManager();
        private void loginButton_Click(object sender, EventArgs e)
        {

            Login aLogin = new Login();
            aLogin.UserName = userTextBox.Text;
            aLogin.Password = passwordTextBox.Text;

            aLoginManager.login(aLogin);
            this.Hide();                           

        }
    }
}
